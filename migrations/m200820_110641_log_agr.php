<?php

use yii\db\Migration;

/**
 * Class m200820_110641_log_agr
 */
class m200820_110641_log_agr extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%log_aggregation}}', [
            'timestamp' => $this->integer()->notNull(),
            'count' => $this->bigInteger()->unsigned()->notNull()->defaultValue(0),
            'delta' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->createIndex('idx-log_aggregation-timestamp', '{{%log_aggregation}}', 'timestamp', true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx-log_aggregation-timestamp', '{{%log_aggregation}}');
        $this->dropTable('{{%log_aggregation}}');
    }
}
