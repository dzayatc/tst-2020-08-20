<?php

use yii\db\Migration;

/**
 * Class m200820_100629_first
 */
class m200820_100629_first extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%log}}', [
            'datetime' => $this->dateTime()->notNull(),
            'status' => $this->smallInteger()->notNull(),
        ]);

        $this->createIndex('idx-log-datetime', '{{%log}}', 'datetime');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx-log-datetime', '{{%log}}');
        $this->dropTable('{{%log}}');
    }

}
