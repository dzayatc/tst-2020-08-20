<?php

namespace app\models\gii;

use Yii;

/**
 * This is the model class for table "log_aggregation".
 *
 * @property int $timestamp
 * @property int $count
 * @property int $delta
 */
class LogAggregation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_aggregation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['timestamp'], 'required'],
            [['timestamp', 'count', 'delta'], 'integer'],
            [['timestamp'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'timestamp' => 'Timestamp',
            'count' => 'Count',
            'delta' => 'Delta',
        ];
    }
}