<?php

namespace app\models\gii;

use Yii;

/**
 * This is the model class for table "log".
 *
 * @property string $datetime
 * @property int $status
 */
class Log extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['datetime'], 'required'],
            [['datetime'], 'safe'],
            [['status'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'datetime' => 'Datetime',
            'status' => 'Status',
        ];
    }
}