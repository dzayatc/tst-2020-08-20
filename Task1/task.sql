/*
1. Спроектировать БД для хранения книг и категорий, к которым эти книги относятся. Одна книга может относится к нескольким категориям.
 У сущности книг, помимо названия, есть свойства: тираж и тип обложки (мягкая, твёрдая).
  Планируется, что БД будет содержать информацию о большом количестве книг.
SQL 1: Вернуть книги, выпущенные в твёрдой обложке, тиражом 5000 экз., которые относятся больше чем к трём категориям
SQL 2: Вернуть пары "книга — книга" и количество общих категорий для этих двух книг, если количество общих категорий больше или равно 10.
*/


CREATE TABLE `book` (
    `id` INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL,
    `circulation` INT(11) UNSIGNED NOT NULL default 0,
    `cover` VARCHAR(30) NOT NULL default 'soft', -- value 'hard' or 'soft'
    KEY `idx-circulation-cover` (circulation, cover)
);

CREATE TABLE `category` (
    `id` INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL
);

CREATE TABLE `book_category` (
    `id` INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `book_id` INT(11) UNSIGNED,
    `category_id` INT(11) UNSIGNED,
    UNIQUE `idx-book-category` (book_id, category_id)
);
----------------------------------------------------------------------------
INSERT INTO `book` (`id`, `name`, `circulation`, `cover`) VALUES
 (NULL, 'book_1', '1000', 'soft'),
 (NULL, 'book_2', '5000', 'soft'),
 (NULL, 'book_3', '10000', 'soft'),
 (NULL, 'book_4', '1000', 'soft'),
 (NULL, 'book_5', '1000', 'hard'),
 (NULL, 'book_6', '5000', 'hard'),
 (NULL, 'book_7', '10000', 'hard'),
 (NULL, 'book_8', '5000', 'hard'),
 (NULL, 'book_9', '6000', 'hard'),
 (NULL, 'book_10', '5000', 'hard');


INSERT INTO `category` (`id`, `name`) VALUES (NULL, 'cat_1'), (NULL, 'cat_2'), (NULL, 'cat_3'), (NULL, 'cat_4'),
 (NULL, 'cat_5'), (NULL, 'cat_6'), (NULL, 'cat_7'), (NULL, 'cat_8'),
 (NULL, 'cat_9'), (NULL, 'cat_10'), (NULL, 'cat_11');

INSERT INTO `book_category` (`id`, `book_id`, `category_id`) VALUES
 (NULL, '1', '1'),
 (NULL, '2', '1'),
 (NULL, '3', '1'),
 (NULL, '4', '1'),
 (NULL, '5', '1'),
 (NULL, '6', '1'),
 (NULL, '7', '1'),
 (NULL, '8', '1'),
 (NULL, '9', '1'),
 (NULL, '10', '1'),
 (NULL, '2', '2'),
 (NULL, '8', '2'),
 (NULL, '9', '2'),
 (NULL, '10', '2'),
 (NULL, '2', '3'),
 (NULL, '8', '3'),
 (NULL, '9', '3'),
 (NULL, '10', '3'),
 (NULL, '2', '4'),
 (NULL, '8', '4'),
 (NULL, '9', '4'),
 (NULL, '10', '4'),
 (NULL, '2', '5'),
 (NULL, '8', '5'),
 (NULL, '9', '5'),
 (NULL, '2', '6'),
 (NULL, '8', '6'),
 (NULL, '9', '6'),
 (NULL, '2', '7'),
 (NULL, '8', '7'),
 (NULL, '9', '7'),
 (NULL, '2', '8'),
 (NULL, '8', '8'),
 (NULL, '9', '8'),
 (NULL, '2', '9'),
 (NULL, '8', '9'),
 (NULL, '9', '9'),
 (NULL, '2', '10'),
-- (NULL, '8', '10'),
 (NULL, '9', '10'),
-- (NULL, '2', '11'),
 (NULL, '8', '11'),
 (NULL, '9', '11');

----------------------------------------------------------------------------
-- sql 1
SELECT `book`.`name`, count(`book_category`.`book_id`) as `count_cat`
FROM `book`
    LEFT JOIN `book_category`
    ON `book_category`.`book_id` = `book`.`id`
WHERE `book`.`cover` = 'hard' AND `book`.`circulation` = 5000
GROUP BY `book`.`id`
HAVING `count_cat` > 3;

-- sql 2
SELECT bc1.`book_id` as 'book_id_1', bc2.`book_id` as 'book_id_2', count(bc1.`book_id`) as `common_count_cat`
FROM `book_category` as bc1
    INNER JOIN `book_category` as bc2
        ON bc1.`book_id` < bc2.`book_id` AND bc1.`category_id` = bc2.`category_id`
GROUP BY bc1.`book_id`, bc2.`book_id`
HAVING `common_count_cat` >= 10;

