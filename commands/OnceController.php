<?php

namespace app\commands;

use app\models\Log;
use app\models\LogAggregation;
use yii\console\Controller;


class OnceController extends Controller
{
    /**
     * Стартовое наполнение базы тестовыми значениями
     */
    public function actionInitData()
    {
        $start = new \DateTime('2020-08-20');

        $interval = new \DateInterval('PT1S');

        // Заполним тестовую выборку примерно на сутки
        for ( $k = 0; $k <= 86400; $k++) {
            $curDateString = $start->format('Y-m-d H:i:s');
            echo $curDateString . "\n";
            // Для 5 кк записей в сутки нужно около 60 записей в секунду
            for ($i = 0; $i <= 60; $i++) {
                $log = new Log();
                $log->datetime = $curDateString;
                $log->status = rand(0,100) < 50 ? Log::STATUS_SIGN_IN : Log::STATUS_SIGN_OUT;
                $log->save();
            }
            $start->add($interval);
        }

        echo 'END' . "\n";
    }

    /**
     * Выполнение агрегации первичных данных.
     *
     * Предполагается что аналогичная агрегация выполняется переодически по свежим данным
     * Период выполнения и прочие детали зависят от других условий использования данных в системе.  Предварительно период выполнения от 1-ой минуты до 1-ого часа
     */
    public function actionLogAggregation()
    {
        $startLog = new \DateTime('now');
        $start = new \DateTime('2020-08-20');
        $interval = new \DateInterval('PT1S');


        $previousCountIn = Log::find()
            ->where(['<','datetime', '2020-08-20'])
            ->andWhere(['status' => Log::STATUS_SIGN_IN])
            ->count();

        $previousCountOut = Log::find()
            ->where(['<','datetime', '2020-08-20'])
            ->andWhere(['status' => Log::STATUS_SIGN_OUT])
            ->count();

        $previousCount = $previousCountIn - $previousCountOut;

        for ( $k = 0; $k <= 86400; $k++) {
            $curDateString = $start->format('Y-m-d H:i:s');
//            echo $curDateString . "\n";// Вывод прогресса

            $curDateTimestamp = $start->getTimestamp();
            $currentCountIn = Log::find()
                ->where(['datetime' => $curDateString])
                ->andWhere(['status' => Log::STATUS_SIGN_IN])
                ->count();

            $currentCountOut = Log::find()
                ->where(['datetime' => $curDateString])
                ->andWhere(['status' => Log::STATUS_SIGN_OUT])
                ->count();

            $delta = $currentCountIn - $currentCountOut;
            $previousCount += $delta;

            if ($previousCount < 0) {
                // Подстраховка. Т.к. данные у нас сурогатные, то такая ситуация вполнен возможна. На принцип алгоритма не влияет.
                $previousCount = 0;
            }

            $agr = new LogAggregation();
            $agr->timestamp = $curDateTimestamp;
            $agr->count = $previousCount;
            $agr->delta = $delta;
            $agr->save();

            $start->add($interval);

        }
        echo 'start_time ' . $startLog->format('Y-m-d H:i:s') . "\n";
        echo 'END ' . (new \DateTime('now'))->format('Y-m-d H:i:s') . "\n";
    }

    /**
     * Выполнение поиска  максимального количиства одновременно присутствующих в заданный период.
     * Период зашит явно для упрощения. Проверки корректности период опущены.
     */
    public function actionStart()
    {
        $startDate = new \DateTime('2020-08-20 00:00:00');
        $endDate = new \DateTime('2020-08-20 23:59:59');

        $max = LogAggregation::find()
            ->where(['>=','timestamp', $startDate->getTimestamp()])
            ->andWhere(['<=','timestamp', $endDate->getTimestamp()])
            ->max('count');

        echo $max . "\n";
    }

}
